\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\begin{document}

\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\center % Center everything on the page

%\textsc{\LARGE University Name}\\[1.5cm] % Name of your university/college
%\textsc{\Large Major Heading}\\[0.5cm] % Major heading such as course name
%\textsc{\large Minor Heading}\\[0.5cm] % Minor heading such as course title

\HRule \\[0.4cm]
{ \huge \bfseries Fitting Atomic Charges to ESP}\\ % Title of your document
\HRule \\[1.5cm]

%\begin{minipage}{0.4\textwidth}
%\begin{flushleft} \large
%\emph{Author:}\\
%John \textsc{Smith} % Your name
%\end{flushleft}
%\end{minipage}
%\begin{minipage}{0.4\textwidth}
%\begin{flushright} \large
%\emph{Supervisor:} \\
%Dr. James \textsc{Smith} % Supervisor's Name
%\end{flushright}
%\end{minipage}\\[4cm]

{\large \today}\\[3cm] % Date, change the \today to a set date if you want to be precise

%\includegraphics{Logo}\\[1cm] % Include a department/university logo - this will require the graphicx package

\vfill % Fill the rest of the page with whitespace

\end{titlepage}

%-------------------------------------------------------------------------------
% TABLE OF CONTENTS
%-------------------------------------------------------------------------------

\tableofcontents % Include a table of contents

\newpage

\section{Introduction}
The fit error $J$ is:
%
\begin{equation}
J = \sum_i^{N_G} \left( V_i - \hat{V}_i\right)^2 + J_\mathrm{c}
\end{equation}
%
where $i$ labels the $N_G$ gridpoints, $V$ is the reference potential, $\hat{V}$ the fitted potential and $J_\mathrm{c}$ the constraint term. The fitted potential is given by:
%
\begin{equation}
\hat{V}_i = \sum_m^{N_A} \frac{q_m}{r_{im}}
\end{equation}
%
where $m$ labels the $N_A$ atoms, $q$ is their ESP charge (to be found) and $r$ the distance between gridpoints and charges. Therefore the fit error becomes:
%
\begin{equation}
J = \sum_i^{N_G} \left( V_i - \sum_m^{N_A} \frac{q_m}{r_{im}} \right)^2 + J_\mathrm{c}
\end{equation}
%
and to find the charges one must derive wrt. them and put the derivative to zero:
%
\begin{equation} \label{E:deriv}
\frac{\partial J}{\partial q_x} = -2\sum_i^{N_G} \left( V_i - \sum_m^{N_A} \frac{q_m}{r_{im}} \right) \frac{1}{r_{ix}} + \frac{\partial J_\mathrm{c}}{\partial q_x} = 0
\end{equation}
%
Defining matrix $\boldsymbol{D}$, with dimension $\left(N_G,N_A\right)$, as:
%
\begin{equation}
D_{im} = \frac{1}{r_{im}}
\end{equation}
%
one can rewrite Eq.~\ref{E:deriv} (without constraints) as:
%
\begin{align}
-2 \sum_i^{N_G} \left( V_i - \sum_m^{N_A} D_{im} q_m \right) D_{ix} = 0
\end{align}
%
which can be rearranged as
%
\begin{align}
2 \sum_i^{N_G} D_{xi}^\dag V_i = 2 \sum_i^{N_G} \sum_m^{N_A} D_{xi}^\dag D_{im} q_m
\end{align}
%
To make everything more compact, one defines $\boldsymbol{G} = \boldsymbol{D}^\dag\boldsymbol{D}$, dimension $\left(N_A,N_A\right)$, and $\boldsymbol{H} = \boldsymbol{D}^\dag \boldsymbol{V}$, dimension $\left(N_A,1\right)$, to obtain, in matrix notation:
%
\begin{equation}
2 \boldsymbol{G} \boldsymbol{q} = 2 \boldsymbol{H}
\end{equation}
%
The solution is found by
%
\begin{equation}
\boldsymbol{G}^{-1} \boldsymbol{H} = \boldsymbol{q}
\end{equation}
%

\section{Constraints}

A single constraint can in general be written as
%
\begin{equation}
\sum_m^{N_A} c_m q_m = d
\end{equation}
%
or, in matrix form:
%
\begin{equation}
\boldsymbol{c}^\dag \boldsymbol{q} = d
\end{equation}
%
The contraints contribution to the fit error can be expressed as
%
\begin{equation} \label{E:cns}
J_\mathrm{c} = \sum_\alpha^{N_c} \lambda^\dag_\alpha \left( \sum_m^{N_A} C^\dag_{\alpha m}q_m - d_\alpha \right)
\end{equation}
%
where index $\alpha$ runs over the $N_c$ constraints, and all the constraints coefficients have been gathered in the matrix $\boldsymbol{C}$, with dimension $\left(N_A,N_c\right)$:
%
\begin{equation}
\boldsymbol{C} = \left[ \begin{array}{cccc} \vdots & \vdots & & \vdots \\ \boldsymbol{c^1} & \boldsymbol{c^2} & \dots & \boldsymbol{c^{N_c}} \\ \vdots & \vdots & & \vdots \end{array} \right]
\end{equation}
%
The derivative of Eq.~\ref{E:cns} wrt. charges will then be
%
\begin{equation}
\frac{\partial J_\mathrm{c}}{\partial q_x} = \sum_\alpha^{N_c} \lambda^\dag_\alpha C^\dag_{\alpha x} = \sum_\alpha^{N_c} C_{x\alpha} \lambda_\alpha
\end{equation}
%
so that
%
\begin{equation} \label{E:deriv_tot}
\frac{\partial J}{\partial q_x} = 2 \sum_m^{N_A} \left( G_{xm} q_m - H_x \right) + \sum_\alpha^{N_c} C_{x\alpha} \lambda_\alpha = 0
\end{equation}
%
which can be rewritten in matrix notation as
%
\begin{equation} \label{E:final}
2\boldsymbol{G} \boldsymbol{q} + \boldsymbol{C} \boldsymbol{\lambda} = 2\boldsymbol{H}
\end{equation}
%
The constraints introduced the lagrange multipliers $\boldsymbol{\lambda}$, so we need to differentiate wrt. them, as in
%
\begin{equation} \label{E:deriv_lam1}
\frac{\partial J}{\partial \lambda_x} = \frac{\partial J_\mathrm{c}}{\partial \lambda_x} = \sum_m^{N_A} C^\dag_{xm} q_m - d_x = 0
\end{equation}
%
In matrix notation, we have:
%
\begin{equation} \label{E:deriv_lam}
\boldsymbol{C}^{\dagger} \boldsymbol{q} = \boldsymbol{d}
\end{equation}
%
Eqs.~\ref{E:final} and \ref{E:deriv_lam} can be satisfied at the same time in a system of equations, as in
%
\begin{equation}
\left[ \begin{array}{cc}
2\boldsymbol{G} & \boldsymbol{C} \\ \boldsymbol{C}^\dag & 0 
\end{array} \right] \left[ \begin{array}{c} \boldsymbol{q} \\ \boldsymbol{\lambda} \end{array} \right] = 
\left[ \begin{array}{c} 2\boldsymbol{H} \\ \boldsymbol{d} \end{array} \right]
\end{equation}
%
The solution is found by
%
\begin{equation}
\left[ \begin{array}{cc}
2\boldsymbol{G} & \boldsymbol{C} \\ \boldsymbol{C}^\dag & 0 
\end{array} \right]^{-1} \left[ \begin{array}{c} 2\boldsymbol{H} \\ \boldsymbol{d} \end{array} \right] = \left[ \begin{array}{c} \boldsymbol{q} \\ \boldsymbol{\lambda} \end{array} \right] 
\end{equation}
%
\subsection{Charge of a fragment}

If one has a constraint on the charge of a fragment, denoted as $Q$, the extra term is:
%
\begin{equation}
J_\mathrm{c} = \lambda \left( \sum_m q_m - Q \right)
\end{equation}
%
The coefficients $c_m$ of each charge will be set to $1$ or $0$, depending on whether atom $m$ belongs to the fragment or not.
The derivative yields:
%
\begin{equation}
\frac{\partial J_\mathrm{c}}{\partial q_x} = \lambda
\end{equation}
%
and $d = Q$.

\subsection{Equivalence of atoms}

The condition of equivalence of the charge $n$ atoms, $q_r = q_s = \dots$, can be split in a system of $n - 1$ pairwise conditions, as in
%
\begin{equation}
J_\mathrm{c} = \lambda \left( q_r - q_s \right)
\end{equation}
%
The coefficients for the charges of the two atoms are set to $c_r = 1$ and $c_s = -1$.

% so that the total derivative is:
% 
% \begin{equation}
% \frac{\partial J}{\partial q_x} = -2\sum_i \left[ V_i - \sum_m \frac{q_m}{r_{im}}\right] \left[ \frac{1}{r_{ix}} \right] + \lambda = 0
% \end{equation}
% 
% and the final equation reads:
% 
% \begin{equation}\label{E:constraints}
% \boldsymbol{G} \boldsymbol{q} + \lambda = \boldsymbol{H}
% \end{equation}

% In order to vectorise this, one needs to consider that the constraint introduced one more variable (not only the fit charges $\left\{q\right\}$ but also the Lagrange multiplier $\lambda$). The variables are collected in the generic variable vector $\boldsymbol{x}$:
% \begin{equation}
% \boldsymbol{x} = \left[ \begin{array}{c} \boldsymbol{q} \\ \lambda \end{array} \right]
% \end{equation}
% so that Equation (\ref{E:constraints}) becomes:
% \begin{equation} \label{E:final1}
% \left[ \begin{array}{cc} \boldsymbol{G} & \boldsymbol{1} \end{array} \right] \left[ \begin{array}{c} \boldsymbol{q} \\ \lambda \end{array} \right] = \left[ \begin{array}{cc} \boldsymbol{G} & \boldsymbol{1} \end{array} \right] \boldsymbol{x} = \boldsymbol{H}
% \end{equation}
% where $\boldsymbol{1}$ is a column vector with 1 on all rows.
% 
% But since there is one more variable ($\lambda$), one also needs to derive wtr. it:
% \begin{equation}
% \frac{\partial J}{\partial \lambda} = \frac{\partial J_\mathrm{c}}{\partial \lambda} = \left[ \sum_m q_m - Q \right] = 0
% \end{equation}
% which can be written in matrix form as:
% \begin{equation} \label{E:final2}
% \boldsymbol{1}^\dag \boldsymbol{q} = Q
% \end{equation}
% 
% Now, one can put Equations (\ref{E:final1}) and (\ref{E:final2}) together as:
% \begin{equation}
% \left[ \begin{array}{cc}
% \boldsymbol{G} & \boldsymbol{1} \\ \boldsymbol{1}^\dag & 0 
% \end{array} \right] \left[ \begin{array}{c} \boldsymbol{q} \\ \lambda \end{array} \right] = 
% \left[ \begin{array}{c} \boldsymbol{H} \\ Q \end{array} \right]
% \end{equation}


\end{document}
