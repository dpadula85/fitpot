#
# In this file specify one group of atoms each line.
# Selections can be expressed in various ways. E.g., to select
# atoms 1 to 3, this can be done as:
#
# 1 2 3
# 1,2,3
# 1-3
#
# The various syntaxes can be combined to express a more complicated selection.
#
19 54-56
