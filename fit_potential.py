#!/usr/bin/env python

import os
import sys
import numpy as np
import argparse as arg
from scipy.spatial import distance


au2ang = 0.5291771


def checkfile(filename):

    if not os.path.isfile(filename):
        print(banner(text='ERROR', ch='#', length=80))
        print(" File %s not found!" % filename)
        sys.exit()


def options():
    '''Defines the options of the script.'''

    parser = arg.ArgumentParser(description='Fits atomic charges to a Molecular ESP',
             formatter_class=arg.ArgumentDefaultsHelpFormatter)

    #
    # Input Options
    #
    inp = parser.add_argument_group("Input Data")
    
    inp.add_argument('-p', '--pot', default=None, type=str,
                     dest="PotFile", help='''Molecular ESP''')

    # inp.add_argument('--heavy', default=False, action="store_true",
    #                  dest="HeavyAtoms", help='''Fit heavy atoms only''')

    inp.add_argument('--totchg', default=None, type=float,
                     dest="TotChg", help='''Total Charge''')

    inp.add_argument('--equiv', default=False, dest="EqAtoms",
                     help='''Equivalence constraints''')

    inp.add_argument('--frag', default=False, dest="FragAtoms",
                     help='''Fragment constraints atoms''')

    inp.add_argument('--fragchg', default=False, dest="FragChgs",
                     help='''Fragment constraints charges''')

    inp.add_argument('--lambda', type=float, default=0.0, dest="Lambda",
                     help='''Regularisation parameter for Least Squares.''')

    #
    # Output Options
    #
    out = parser.add_argument_group("Output Options")
    
    out.add_argument('-v', '--verbosity',
                     default=0, action="count", dest="Verb",
                     help='''Verbosity level''')

    args = parser.parse_args()
    Opts = vars(args)

    return Opts


def read_gesp(gespfile):

    checkfile(gespfile)
    atoms = []
    struct = []
    chgs = []
    grid = []
    pots = []
    with open(gespfile) as f:
        for line in f:

            if "CHARGE " in line:
                data = line.strip().split()
                chg = int(data[2])
                mult = int(data[-1])

            if "ATOMIC COORDINATES" in line:
                data = line.strip().split()
                natoms = int(data[-1])
                line = next(f)
                i = 0

                while i < natoms:
                    data = line.strip().replace("D", "E").split()
                    atoms.append(data[0])
                    struct.append(list(map(float, data[1:4])))
                    chgs.append(float(data[-1]))
                    line = next(f)
                    i += 1

            if "DIPOLE MOMENT" in line:
                line = next(f)
                data = line.strip().replace("D", "E").split()
                dip = list(map(float, [data[1], data[3], data[5]]))

            if "VALUES AND GRID" in line:
                data = line.strip().split()
                npts = int(data[-1])
                line = next(f)
                j = 0

                while j < npts:
                    data = line.strip().replace("D", "E").split()
                    pots.append(float(data[0]))
                    grid.append(list(map(float, data[1:])))

                    try:
                        line = next(f)

                    except StopIteration:
                        break

                    j += 1

    atoms = np.array(atoms)
    struct = np.array(struct)
    chgs = np.array(chgs)
    dip = np.array(dip)
    grid = np.array(grid)
    pots = np.array(pots).reshape(-1,1)

    return atoms, chg, struct, chgs, dip, grid, pots


def read_sel(filename):

    checkfile(filename)
    strings = []
    with open(filename) as f:
        for line in f:
            if line.startswith('#') or not line.strip():
                continue
            else:
                strings.append(line.strip())

    result = []
    for string in strings:
        string = string.replace(',', ' ')
        idxs = extend_compact_list(string)
        idxs = list(map(lambda x: x - 1, idxs))
        result.append(idxs)

    return result


def extend_compact_list(idxs):

    extended = []

    # Uncomment this line if idxs is a string and not a list
    idxs = idxs.split()

    for idx in idxs:

        idx = idx.replace(' ', '')
        to_extend = idx.split('-')

        if len(to_extend) > 1:
            sel = list(map(int, to_extend))
            extended += range(sel[0],sel[1]+1,1)

        else:
            extended.append(int(idx))
    
    return extended


def gen_eq_cns_mat(cns, na):

    n = len(cns)
    C = np.zeros((na,n - 1))
    d = np.zeros((n - 1,1))

    for i, j in enumerate(range(n - 1)):
        idx1, idx2 = cns[j:j + 2]
        C[idx1,i] = 1
        C[idx2,i] = -1

    return C, d


def format_selection(intlist):

    s = ''
    for i in intlist:
        s += '%3d ' % (i + 1) 

    return s


def print_dict(opts_dict, title=None, outstream=None):

    if outstream:
        sys.stdout = outstream

    if not title:
        title = "OPTIONS"

    print(banner(text=title,ch="=", length=60))

    fmt = " %-20s %-20s"
    for k, v in sorted(list(opts_dict.items())):

        if type(v) is str:
            pass

        if type(v) is int or type(v) is float:
            v = str(v)

        if type(v) is list:
            v = ', '.join(list(map(str, v)))

        if type(v) is dict:
            v = ', '.join(list(map(str, v.keys())))

        print(fmt % (k, v))

    print(banner(ch="=", length=60))
    print()

    return


def banner(text=None, ch='=', length=78):
    """Return a banner line centering the given text.
    
        "text" is the text to show in the banner. None can be given to have
            no text.
        "ch" (optional, default '=') is the banner line character (can
            also be a short string to repeat).
        "length" (optional, default 78) is the length of banner to make.

    Examples:
        >>> banner("Peggy Sue")
        '================================= Peggy Sue =================================='
        >>> banner("Peggy Sue", ch='-', length=50)
        '------------------- Peggy Sue --------------------'
        >>> banner("Pretty pretty pretty pretty Peggy Sue", length=40)
        'Pretty pretty pretty pretty Peggy Sue'
    """
    if text is None:
        return ch * length

    elif len(text) + 2 + len(ch)*2 > length:
        # Not enough space for even one line char (plus space) around text.
        return text

    else:
        remain = length - (len(text) + 2)
        prefix_len = remain / 2
        suffix_len = remain - prefix_len
    
        if len(ch) == 1:
            prefix = ch * int(prefix_len)
            suffix = ch * int(suffix_len)

        else:
            prefix = ch * int((prefix_len/len(ch)) + ch[:prefix_len%len(ch)])
            suffix = ch * int((suffix_len/len(ch)) + ch[:suffix_len%len(ch)])

        return prefix + ' ' + text + ' ' + suffix


def main(Opts):

    if Opts['Verb'] > 0:
        print_dict(Opts)

    #
    # Read input data
    #
    QMatoms, chg, QMcoords, chgs, dip, grid, V = read_gesp(Opts['PotFile'])

    if Opts['TotChg'] is not None:
        chg = Opts['TotChg']

    #
    # Parse equivalent atoms
    #
    if Opts['EqAtoms']:
        eq_cns = read_sel(Opts['EqAtoms'])
    else:
        eq_cns = []

    #
    # Parse fragments
    #
    if Opts['FragAtoms']:
        frag_cns = read_sel(Opts['FragAtoms'])
    else:
        frag_cns = []

    if Opts['FragChgs']:
        checkfile(Opts['FragChgs'])
        d_frag = np.loadtxt(Opts['FragChgs']).reshape(-1,1)

    # #
    # # Filter out H atoms to fit the potential
    # #
    # if Opts['HeavyAtoms']:
    #     idxs = np.where(QMatoms == "H")
    #     QMcoords = np.delete(QMcoords, idxs, axis=0)
    #     QMatoms = np.delete(QMatoms, idxs)

    #
    # Compute the matrix of the system
    #
    D = distance.cdist(grid, QMcoords)
    D = 1 / D

    #
    # Build G and H matrices
    #
    G = np.dot(D.T, D)
    H = np.dot(D.T, V)

    #
    # Add regularisation term to G
    # lambda = 0: ESP charges
    # lambda > 0: RESP charges
    #
    n = G.shape[0]
    lam = Opts['Lambda']
    G = G + lam * np.eye(n)

    #
    # Get number of atoms and of constraints
    # We always have 1 constraint on the total charge
    # An equivalence constraint involving N atoms needs N - 1 equations to be
    # formulated
    #
    na = G.shape[0]
    nc_eq = list(map(len, eq_cns))
    nc_eq = np.sum(list(map(lambda x: x - 1, nc_eq)))
    nc_frag = len(frag_cns)
    nc = nc_eq + nc_frag + 1
    nc = int(nc)

    #
    # Generate constraints matrix
    #

    # Total charge
    C = np.ones((na,1))
    d = chg

    # Equivalence constraints
    if Opts['Verb'] > 0:
        print(banner(text="EQUIVALENT ATOMS", ch='=', length=60))
        print(banner(ch='-', length=60))
        print(" %-s" % "Atoms")
        print(banner(ch='-', length=60))

    for cns in eq_cns:
        Ci, di = gen_eq_cns_mat(cns, na)
        C = np.hstack((C, Ci))
        d = np.vstack((d, di))
        if Opts['Verb'] > 0:
            print(format_selection(cns))

    if Opts['Verb'] > 0:
        print(banner(ch='=', length=60))
        print()

    # Fragments constraints
    if Opts['Verb'] > 0:
        print(banner(text="FRAGMENT CHARGES", ch='=', length=60))
        print(banner(ch='-', length=60))
        print(" %-33s %10s" % ("Atoms", "Charge"))
        print(banner(ch='-', length=60))

    C_frag = np.zeros((na, nc_frag))
    try:
        d_frag
    except NameError:
        d_frag = np.zeros((nc_frag, 1))

    for i, cns in enumerate(frag_cns):
        C_frag[cns,i] = 1
        if Opts['Verb'] > 0:
            sel = format_selection(cns)
            frchg = d_frag[i,0]
            print("%-36s %10.6f" % (sel, frchg))

    # Stack Constraints
    C = np.hstack((C, C_frag))
    d = np.vstack((d, d_frag))

    if Opts['Verb'] > 0:
        print(banner(ch='=', length=60))
        print()

    #
    # Append System and Constraints Matrices
    #
    Gnew = np.zeros((na + nc,na + nc))
    Gnew[:na,:na] = 2 * G
    Gnew[na:na+nc,:na] = C.T
    Gnew[:na,na:na+nc] = C
    G = Gnew

    H = np.vstack((2 * H, d))

    #
    # Compute fitted chgs
    # The last nc elements are lagrange multipliers for constraints
    #
    q = np.linalg.solve(G, H)

    #
    # Compute fitted potential and RMSE
    #
    V_hat = np.dot(D, q[:-nc])
    rms = np.sqrt(np.mean((V - V_hat)**2))

    #
    # Print results
    #
    print(banner(text='FITTED CHARGES', ch='=', length=60))

    if Opts['Verb'] > 1:
        QMcoords *= au2ang
        print(" %d" % len(QMatoms))
        print()

    for i in range(len(QMatoms)):
        
        if Opts['Verb'] > 1:
            print(" %-10s %10.6f %10.6f %10.6f %10.6f" % (QMatoms[i],
                  QMcoords[i,0], QMcoords[i,2], QMcoords[i,2], q[i]))
        else:
            print(" %-10s %10.6f" % (QMatoms[i], q[i]))

    print(banner(ch='-', length=60))

    print(" %-10s %10.6f" % ("Total Chg.", np.sum(q[:-nc])))
    print(" %-10s %10.6f" % ("RMS Error", rms))

    print(banner(ch='-', length=60))
    print(banner(ch='=', length=60))

    return


if __name__ == '__main__':

    Opts = options()
    main(Opts)
